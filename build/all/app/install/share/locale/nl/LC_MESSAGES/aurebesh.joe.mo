��          �      l      �     �     �     �                 	   #     -  
   <     G  �   S                %     +     4     F     V  
   g  	   r  ~  |     �                     %     6     B     O     a     o  �   ~     n  	   z     �     �     �     �     �     �     �     
                                                              	                               About About Aurebesh About This App Aurebesh Aurebesh Chart Check Clear All Do. Or do not. Enter Text Impressive. Practice reading Aurebesh words from the Star Wars universe. Input the word that you see on the screen and press 'Check' to see if it is correct. Press 'Skip' to test a different word instead. Report Bugs Skip Space Streak:  Switch to Letters Switch to Words There is no try. Translator Version:  Project-Id-Version: aurebesh.joe
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-03-19 22:51+0000
PO-Revision-Date: 2021-03-21 12:13+0100
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
Last-Translator: Heimen Stoffels <vistausss@outlook.com>
Plural-Forms: nplurals=2; plural=(n != 1);
Language: nl
 Over Over Aurebesh Over deze app Aurebesh Aurebesh-grafiek Controleren Alles wissen Doe het. Of niet. Voer tekst in Indrukwekkend. Leer de Aurebesh-taal, uit het Star Wars-universum, door middel van oefeningen. Voer het woord in dat je op het scherm ziet staan en druk op 'Controleren' om te zien of je antwoord klopt. Druk op 'Overslaan' om een ander woord te proberen. Bugs melden Overslaan Ruimte Record:  Letterweergave Woordweergave 'Proberen' bestaat niet. Vertaler Versie:  