��          �      l      �     �     �     �                 	   #     -  
   <     G  �   S                %     +     4     F     V  
   g  	   r    |  	   �     �     �     �     �  	   �     �          !     4  �   D     6     L     S     Z     k     �     �  
   �  
   �     
                                                              	                               About About Aurebesh About This App Aurebesh Aurebesh Chart Check Clear All Do. Or do not. Enter Text Impressive. Practice reading Aurebesh words from the Star Wars universe. Input the word that you see on the screen and press 'Check' to see if it is correct. Press 'Skip' to test a different word instead. Report Bugs Skip Space Streak:  Switch to Letters Switch to Words There is no try. Translator Version:  Project-Id-Version: aurebesh.joe
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-03-19 22:51+0000
PO-Revision-Date: 2021-03-21 20:26+0100
Last-Translator: Anne017
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 À propos À propos d'Aurebesh À propos de cette application Aurebesh Tableau Aurebesh Vérifier Tout effacer Fais-le. Ou ne le fais pas. Saisissez un texte Impressionnant. Pratiquez la lecture des mots en Aurebesh de l'univers de Star Wars. Saisissez le mot que vous voyez à l'écran et appuyez sur « Vérifier » pour voir si celui-ci est correct. Appuyez sur « Passer » pour tester un autre mot à la place. Signaler un problème Passer Espace Trait d'union :  Basculer sur les lettres Basculer sur les mots Il n'y a pas d'essai. Traducteur Version :  