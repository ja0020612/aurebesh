// Translator.qml
//
// This file is part of the Aurebesh application.
//
// Copyright (c) 2017 
//
// Maintained by Joe (@exar_kun) <joe@ubports.com>
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.4
import Ubuntu.Components 1.3
import "modules"


Page {
    id: translatorPage
    title: i18n.tr("Translator")
    header: DefaultHeader {}

    ScrollView {
        id: scroll
        anchors {
            fill: parent
            topMargin: translatorPage.header.height
        }

        Column {
            id: traslateColumn
            width: scroll.width
            spacing: units.gu(1)
            anchors.horizontalCenter: parent.horizontalCenter

            Label {
                id: mode
                text: i18n.tr("Enter Text")
                anchors.horizontalCenter: parent.horizontalCenter
            }
 
            TextField {

               FontLoader {
                   id: abFont
                   source: "../assets/aurebesh.ttf"
               }

                id: auText
                cursorVisible: true 
                width: parent.width - units.gu(3)
                font.family: abFont.name
                font.pixelSize: 90
                text: ""
                anchors.horizontalCenter: parent.horizontalCenter
                readOnly: true
            }

           TextField {
                id: abText
                cursorVisible: true 
                width: parent.width - units.gu(3)
                font.family: "Courier"
                font.pixelSize: 100
                text: ""
                anchors.horizontalCenter: parent.horizontalCenter
                readOnly: true
            }

            Row {
                anchors.horizontalCenter: parent.horizontalCenter
                Image {
                    id: qKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/q.svg"

                    MouseArea {
                        id: mouseQ
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            qKey.opacity = 0.5
                            auText.text += "q"
                            abText.text += "q"
                        }
                        onReleased: {
                            qKey.opacity = 1.0
                        }
                    }
                }
                Image {
                    id: wKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/w.svg"

                    MouseArea {
                        id: mouseW
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            wKey.opacity = 0.5
                            auText.text += "w"
                            abText.text += "w"
                        }
                        onReleased: {
                            wKey.opacity = 1.0
                        }
                    }
                 }
                Image {
                    id: fKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/f.svg"

                    MouseArea {
                        id: mouseF
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            fKey.opacity = 0.5
                            auText.text += "f"
                            abText.text += "f"
                        }
                        onReleased: {
                            fKey.opacity = 1.0
                        }
                    }
                 }
                Image {
                    id: pKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/p.svg"

                    MouseArea {
                        id: mouseP
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            pKey.opacity = 0.5
                            auText.text += "p"
                            abText.text += "p"
                        }
                        onReleased: {
                            pKey.opacity = 1.0
                        }
                    }
                 }
                Image {
                    id: gKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/g.svg"

                    MouseArea {
                        id: mouseG
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            gKey.opacity = 0.5
                            auText.text += "g"
                            abText.text += "g"
                        }
                        onReleased: {
                            gKey.opacity = 1.0
                        }
                    }
                 }
                Image {
                    id: jKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/j.svg"

                    MouseArea {
                        id: mouseJ
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            jKey.opacity = 0.5
                            auText.text += "j"
                            abText.text += "j"
                        }
                        onReleased: {
                            jKey.opacity = 1.0
                        }
                    }
                 }
                Image {
                    id: lKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/l.svg"

                    MouseArea {
                        id: mouseL
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            lKey.opacity = 0.5
                            auText.text += "l"
                            abText.text += "l"
                        }
                        onReleased: {
                            lKey.opacity = 1.0
                        }
                    }
                 }
                Image {
                    id: uKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/u.svg"

                    MouseArea {
                        id: mouseu
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            uKey.opacity = 0.5
                            auText.text += "u"
                            abText.text += "u"
                        }
                        onReleased: {
                            uKey.opacity = 1.0
                        }
                    }
                 }
                Image {
                    id: yKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/y.svg"

                    MouseArea {
                        id: mousey
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            yKey.opacity = 0.5
                            auText.text += "y"
                            abText.text += "y"
                        }
                        onReleased: {
                            yKey.opacity = 1.0
                        }
                    }
                 }
            }
            Row {
                anchors.horizontalCenter: parent.horizontalCenter
                Image {
                    id: aKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/a.svg"

                    MouseArea {
                        id: mousea
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            aKey.opacity = 0.5
                            auText.text += "a"
                            abText.text += "a"
                        }
                        onReleased: {
                            aKey.opacity = 1.0
                        }
                    }
                 }
                Image {
                    id: rKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/r.svg"

                    MouseArea {
                        id: mouser
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            rKey.opacity = 0.5
                            auText.text += "r"
                            abText.text += "r"
                        }
                        onReleased: {
                            rKey.opacity = 1.0
                        }
                    }
                 }
                Image {
                    id: sKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/s.svg"

                    MouseArea {
                        id: mouses
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            sKey.opacity = 0.5
                            auText.text += "s"
                            abText.text += "s"
                        }
                        onReleased: {
                            sKey.opacity = 1.0
                        }
                    }
                 }
                Image {
                    id: tKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/t.svg"

                    MouseArea {
                        id: mouset
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            tKey.opacity = 0.5
                            auText.text += "t"
                            abText.text += "t"
                        }
                        onReleased: {
                            tKey.opacity = 1.0
                        }
                    }
                 }
                Image {
                    id: dKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/d.svg"

                    MouseArea {
                        id: moused
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            dKey.opacity = 0.5
                            auText.text += "d"
                            abText.text += "d"
                        }
                        onReleased: {
                            dKey.opacity = 1.0
                        }
                    }
                 }
                Image {
                    id: hKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/h.svg"

                    MouseArea {
                        id: mouseh
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            hKey.opacity = 0.5
                            auText.text += "h"
                            abText.text += "h"
                        }
                        onReleased: {
                            hKey.opacity = 1.0
                        }
                    }
                 }
                Image {
                    id: nKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/n.svg"

                    MouseArea {
                        id: mousen
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            nKey.opacity = 0.5
                            auText.text += "n"
                            abText.text += "n"
                        }
                        onReleased: {
                            nKey.opacity = 1.0
                        }
                    }
                 }
                Image {
                    id: eKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/e.svg"

                    MouseArea {
                        id: mousee
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            eKey.opacity = 0.5
                            auText.text += "e"
                            abText.text += "e"
                        }
                        onReleased: {
                            eKey.opacity = 1.0
                        }
                    }
                 }
                Image {
                    id: iKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/i.svg"

                    MouseArea {
                        id: mousei
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            iKey.opacity = 0.5
                            auText.text += "i"
                            abText.text += "i"
                        }
                        onReleased: {
                            iKey.opacity = 1.0
                        }
                    }
                 }
                Image {
                    id: oKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/o.svg"

                    MouseArea {
                        id: mouseo
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            oKey.opacity = 0.5
                            auText.text += "o"
                            abText.text += "o"
                        }
                        onReleased: {
                            oKey.opacity = 1.0
                        }
                    }
                 }
            }

            Row {
                anchors.horizontalCenter: parent.horizontalCenter
                Image {
                    id: zKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/z.svg"

                    MouseArea {
                        id: mousez
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            zKey.opacity = 0.5
                            auText.text += "z"
                            abText.text += "z"
                        }
                        onReleased: {
                            zKey.opacity = 1.0
                        }
                    }
                 }
                Image {
                    id: xKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/x.svg"

                    MouseArea {
                        id: mousex
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            xKey.opacity = 0.5
                            auText.text += "x"
                            abText.text += "x"
                        }
                        onReleased: {
                            xKey.opacity = 1.0
                        }
                    }
                 }
                Image {
                    id: cKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/c.svg"

                    MouseArea {
                        id: mousec
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            cKey.opacity = 0.5
                            auText.text += "c"
                            abText.text += "c"
                        }
                        onReleased: {
                            cKey.opacity = 1.0
                        }
                    }
                 }
                Image {
                    id: vKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/v.svg"

                    MouseArea {
                        id: mousev
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            vKey.opacity = 0.5
                            auText.text += "v"
                            abText.text += "v"
                        }
                        onReleased: {
                            vKey.opacity = 1.0
                        }
                    }
                 }
                Image {
                    id: bKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/b.svg"

                    MouseArea {
                        id: mouseb
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            bKey.opacity = 0.5
                            auText.text += "b"
                            abText.text += "b"
                        }
                        onReleased: {
                            bKey.opacity = 1.0
                        }
                    }
                 }
                Image {
                    id: kKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/k.svg"

                    MouseArea {
                        id: mousek
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            kKey.opacity = 0.5
                            auText.text += "k"
                            abText.text += "k"
                        }
                        onReleased: {
                            kKey.opacity = 1.0
                        }
                    }
                 }
                Image {
                    id: mKey
                    width: scroll.width * 0.1
                    height: units.gu(6)
                    source: "../assets/keys/m.svg"

                    MouseArea {
                        id: mousem
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            mKey.opacity = 0.5
                            auText.text += "m"
                            abText.text += "m"
                        }
                        onReleased: {
                            mKey.opacity = 1.0
                        }
                    }
                 }
            }
            Row {
                anchors.horizontalCenter: parent.horizontalCenter
                Button {
                    id: clearBut
                    text: i18n.tr("Clear All")
                    onClicked: {
                        auText.text = ""
                        abText.text = ""
                    }
                }
                Button {
                    id: spacebar
                    width: units.gu(20)
                    text: i18n.tr("Space")
                    onClicked: {
                        auText.text += " "
                        abText.text += " "
                    }
                }
                Button {
                    id: backspace
                    text: "⌫"
                    onClicked: {
                        auText.text = auText.text.substring(0, auText.text.length-1)
                        abText.text = abText.text.substring(0, abText.text.length-1)
                    }
                }

            }

        }
    }
}
